FROM gitlab/gitlab-runner:latest

LABEL maintainer="Greg Farr"

ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint"]

CMD ["unregister", "--all-runners"]

CMD ["register", "--non-interactive", "--url=https://gitlab.com/", "--registration-token=NpEGMgfPzbbHQty2tSTj", "--executor=docker", "--docker-image=alpine:3", "--description=docker-runner-jboss", "--tag-list=docker,jboss", "--run-untagged", "--locked=false"]

CMD ["verify"]